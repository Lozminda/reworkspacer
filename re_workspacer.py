#! /home/hp-350/.pyenv/versions/3.8.7/bin/python3

import json
import subprocess
# Get running applications and their workspaces using wmctrl

def get_running_apps():
    result = subprocess.run(['wmctrl', '-lx'], capture_output=True, text=True)
    lines = result.stdout.strip().split('\n')
    apps = str()
    for line in lines:
        parts = line.split()
        workspace = parts[1]
        app_name = ' '.join(parts[4:])
        apps += f"'app': {app_name}, 'workspace': {workspace}\n"
    with open("/home/hp-350/apps.txt", 'w') as file:
        file.write(apps)

# Generate bash script to restore application setup
# def generate_restore_script(apps):
#     script = '#!/bin/bash\n\n'
#     for app in apps:
#         script += f'wmctrl -s {app["workspace"]} -R "{app["app"]}"\n'
#     return script

# # Save the restore script to a file
# def save_restore_script(script, filename):
#     with open(filename, 'w') as file:
#         file.write(script)
#     subprocess.run(['chmod', '+x', filename])

if __name__ == '__main__':

    # Get running applications and their workspaces
    get_running_apps()

# Generate restore script
# restore_script = generate_restore_script(running_apps)

# Save restore script to a file
# save_restore_script(restore_script, 'restore_apps.sh')